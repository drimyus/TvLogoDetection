import numpy as np
import cv2

cap = cv2.VideoCapture('./video/fox.mp4')


logo = cv2.imread('./logos/fox_logo.jpg', 0)
logo = cv2.resize(logo, (40, 50))
cv2.imshow("logo", logo)

w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

fourcc = cv2.VideoWriter_fourcc('D', 'I', 'V', 'X')
out = cv2.VideoWriter('result.avi', fourcc, 25, (h, w))
# fourcc = cv2.VideoWriter_fourcc(*'MP4V')
# out = cv2.VideoWriter('result.mp4', fourcc, 25, (h, w))

while cap.isOpened():
    ret, frame = cap.read()
    frame = cv2.resize(frame, None, fx=1, fy=1, interpolation=cv2.INTER_CUBIC)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    w, h = logo.shape[::-1]
    res = cv2.matchTemplate(gray, logo, cv2.TM_CCOEFF_NORMED)
    threshold = 0.7
    loc = np.where(res >= threshold)
    for pt in zip(*loc[::-1]):
        logo_img = frame[pt[1]:pt[1]+h, pt[0]:pt[0]+w]
        cv2.rectangle(frame, pt, (pt[0] + w, pt[1] + h), (0, 255, 0), 1)

        cv2.imshow('logo detection', logo_img)
    cv2.imshow('frame', frame)
    out.write(frame)

    if cv2.waitKey(1) & 0xFF == ord('n'):
        pos = cap.get(cv2.CAP_PROP_POS_FRAMES)
        cap.set(cv2.CAP_PROP_POS_FRAMES, pos+1000)
    elif cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
out.release()
cv2.destroyAllWindows()
